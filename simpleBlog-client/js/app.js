App = Ember.Application.create({
    // Basic logging, e.g. "Transitioned into 'post'"
    LOG_TRANSITIONS: true,
    LOG_ACTIVE_GENERATION: true
});

App.Ajax = Ember.Mixin.create({
    token: void 0,
    getJSON: function(url, options) {
        queryOptions = {};

        type = (options != null ? options.type : void 0) || "GET";
        headers = {};


        return Em.$.ajax({
            url: "http://localhost:8080" + url,
            type: type,
            contentType: "application/json",
            headers: headers,
            dataType: 'json',
            beforeSend: function(){
                Em.$('.spinner').show();
            },
            complete: function(){
                Em.$('.spinner').hide();
            }
        });
    },
    postJSON: function(url, data, options) {
        var auth, contentType, headers, _ref;
        auth = false;
        if (options != null ? options.auth : void 0) {
            auth = true;
        }
        headers = {};
        contentType = "application/json";
        if (auth) {
            headers = {
                "Authorization": "Bearer " + localStorage.getCacheItem('access_token')
            };
        }
        return Em.$.ajax({
            url: "http://localhost:8080" + url,
            type: "POST",
            data: data,
            contentType: contentType,
            headers: headers,
            dataType: 'json',
            beforeSend: function(){
                $('.spinner').show()
            },
            complete: function() {
                $('.spinner').hide();
            }
        });
    },
    deleteJSON: function(url, options) {
        return this.getJSON(url, {
            type: "DELETE"
        });
    },
    getToken: function() {
        return App.__container__.lookup('controller:login').get('token');
    }
});

App.Router.map(function() {

    this.resource('index',{path : '/'},function(){

        this.resource('post', { path:'/posts/:post_id' });
        this.resource('newPost' , {path : 'post/new'});
        this.resource('login' , {path : 'login'});
        this.resource('signup' , {path : 'signup'});
        this.resource('search' , {path : 'search'});

    });
});


App.IndexController = Ember.ArrayController.extend({
    sortProperties : ['createdAt'],
    sortAscending : false,

    newData : [],
    refresh : function(){
        var postIdArr = _.map(this.get('model'),function(post){
           return post.id
        })
        for(i=0;i < this.newData.length;i++){

            if(!_.contains(postIdArr,this.newData[i].id)){
                this.get('model').pushObject(this.newData[i]);
            }
        }
    }.observes('newData')
});

App.IndexRoute = Ember.Route.extend(App.Ajax,{
    actions: {
        postsModified: function (data) {
            this.model();
        }
    },
    model : function(){
        _this = this;
        return this.getJSON('/api/posts').then(function(result) {
            _this.set('pol',App.Pollster.create());
            _this.get('pol').start();

            return result
        });
    }
});

App.PostRoute = Ember.Route.extend(App.Ajax,{
    model : function(params){
        return this.getJSON('/api/posts/'+params.post_id).then(function(result) {
            return result
        })
    },
    setupController : function(controller, model){
        controller.set("model", model);
    }
});

App.NewPostRoute = Ember.Route.extend(App.Ajax,{
    beforeModel: function(transition) {

        var token = localStorage.getCacheItem("access_token");
        if (token == null) {

            localStorage.setCacheItem("transition_to",transition.targetName,{ hours : 1});
            this.transitionTo('login');
            Bootstrap.NM.push('Please login to post' , 'info');


        }
    },
    model : function(params){
        post = Ember.Object.create({
            subject : "",
            post   : ""
        })
        return post;
    }
});

App.LoginRoute = Ember.Route.extend(App.Ajax,{
    beforeModel : function(params){

        var loginController = this.controllerFor("login");

        user = Ember.Object.create({
            email : "",
            password   : ""
        })
        loginController.set('model',user)
    }
});

App.SignupRoute = Ember.Route.extend(App.Ajax,{
    model : function(params){

        var user = App.User.create()
        return user;
    }
});
App.ApplicationController = Ember.ObjectController.extend(App.Ajax,{
    search : undefined,
    init: function() {
        if(localStorage.getCacheItem('access_token') != null) {
            this.set('userName', localStorage.getCacheItem('email'));
            this.set('isLogged', true);
        }
    },
    content : {
        userName : undefined,
        isLogged : undefined
    },
    actions: {
        logout: function () {

            localStorage.removeItem("access_token");

            this.set('userName', undefined);
            this.set('isLogged', undefined);

            this.transitionToRoute('index');

        },

        search: function (data) {
            var _this = this;
            if(!Em.$("#searchForm").valid()) return;
            this.getJSON('/api/posts/search?query=' + _this.get('search')).then(function(result) {

                _this.controllerFor('search').set('model', result);
                _this.transitionToRoute('search');
            });
        }

    }
});

App.LoginController = Ember.ObjectController.extend(App.Ajax,{

    actions :{
        login : function(){

            if(!Em.$("#loginForm").valid()) return;

            var user = this.get('model');

            var authData = "username=" + (user.get('email')) + "&password=" + (user.get('credentials')) + "&client_secret=" +App.client_secret+ "&grant_type=password&client_id=" +App.client_id;

            var _this = this;
            //URL encoded post request
            Em.$.ajax({
                url : "http://localhost:8080/api/token",
                type: "POST",
                data: authData
            }).then(function(result) {

                if(result.access_token == null){
                    return;
                    //handle log error
                }
                _this.controllerFor('application').set('userName', user.email);
                _this.controllerFor('application').set('isLogged', 'true');

                localStorage.setCacheItem("access_token", result.access_token, { hours : 1 });
                localStorage.setCacheItem("email", user.get('email') , { hours : 1 });


                var transition = localStorage.getCacheItem("transition_to");

                if(transition!=null){
                    //return to previous requested protected page
                    _this.transitionToRoute(transition);
                }else
                    _this.transitionToRoute('index');
                    Bootstrap.NM.push('Hello ' + user.email , 'info');

            })
        }
    }
});

App.SignupController = Ember.ObjectController.extend(App.Ajax,{

    actions :{
        signup : function(){

            if(!Em.$("#signupForm").valid()) return;

            var user = this.get('model');

            var userDTO = {
                email : user.get('email'),
                name   : user.get('name'),
                credentials   : user.get('credentials')

            }

            var _this = this;
            return this.postJSON('/api/users', JSON.stringify(userDTO)).then(function(result) {
                //return result
                _this.transitionToRoute('login');
                Bootstrap.NM.push('User created. Please login', 'info');
            })
        }
    }
});

App.SearchController = Ember.ArrayController.extend({

});

App.NewPostController = Ember.ObjectController.extend(App.Ajax,{

    actions :{
        save : function(){

            if(!Em.$("#newPostForm").valid()) return;

            var post = this.get('model');
            var postDTO = JSON.stringify(post);
            _this = this;
            return this.postJSON('/api/posts',postDTO,{auth:true}).then(function(result) {

                //_this.send('postsModified');
                _this.transitionTo('index');

                Bootstrap.NM.push('Post Created', 'info');
                return result
            })
        }
    }
});


App.User = Ember.Object.extend({

    email : "",
    name  : "",
    password   : ""
})

App.client_secret = "outbrain";
App.client_id = "outbrain";

Ember.Handlebars.helper('format-date', function(date){
    return moment(date).fromNow();
});

App.Pollster = Ember.Route.extend(App.Ajax,{
    start: function(){

        this.timer = setInterval(this.onPoll.bind(this), 5000);
    },
    stop: function(){
        clearInterval(this.timer);
    },
    onPoll: function(){
        _this = this
        this.getJSON('/api/posts').then(function(result) {
            App.__container__.lookup('controller:index').set('newData', result);
        })
    }
});



App.highlight = function (element, errorClass, validClass) {
    Em.$(element).parents("div").addClass("has-error");
};

App.unhighlight = function (element, errorClass, validClass) {
    Em.$(element).parents("div").removeClass("has-error");
};

App.LoginValidationsView = Ember.View.extend({
    template: Ember.Handlebars.compile(''),
    didInsertElement : function() {

        Em.$("#loginForm").validate({
            rules: {
                password: {
                    required: true,
                    minlength: 2
                },
                email: {
                    required: true,
                    email: true
                }
            },
            errorElement: "span",
            highlight: App.highlight,
            unhighlight: App.unhighlight

        });
    }
});

App.SignupValidationsView = Ember.View.extend({
    template: Ember.Handlebars.compile(''),
    didInsertElement : function() {

        Em.$("#signupForm").validate({
            rules: {
                password: {
                    required: true,
                    minlength: 2
                },
                email: {
                    required: true,
                    email: true
                },
                name: {
                    required: true,
                    minlength: 4
                }
            },
            errorElement: "span",
            highlight: App.highlight,
            unhighlight: App.unhighlight

        });
    }
});

App.NewPostValidationsView = Ember.View.extend({
    template: Ember.Handlebars.compile(''),
    didInsertElement : function() {

        Em.$("#newPostForm").validate({
            rules: {
                subject: {
                    required: true,
                    minlength: 5
                },
                post: {
                    required: true,
                    minlength: 5
                }
            },
            errorElement: "span",
            highlight: App.highlight,
            unhighlight: App.unhighlight

        });
    }
});

App.SearchValidationsView = Ember.View.extend({
    template: Ember.Handlebars.compile(''),
    didInsertElement : function() {

        Em.$("#searchForm").validate({
            rules: {
                query: {
                    required: true,
                    minlength: 3
                }
            },
            errorElement: "span",
            highlight: App.highlight,
            unhighlight: App.unhighlight,
            errorPlacement: function(error, element) {}

        });
    }
});

App.AnItemView = Ember.View.extend({
    template: Ember.Handlebars.compile(''),
    didInsertElement : function(){
        Em.$( ".post" ).click(function() {
            $("html, body").animate({ scrollTop: 0 }, "slow");
        });
    }
});




