package com.example.common;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.inject.Singleton;

/**
 * Encryption utilities
 */
@Singleton
public class EncryptionUtils {
    private static HashFunction sha1 = Hashing.sha1();
    private static HashFunction md5 = Hashing.md5();

    public static enum HASH_TYPE {
        SHA1, MD5
    }

    //TODO: Increase encryption strength by supporting salt.
    public String encrypt(String clearText, HASH_TYPE hashType) {
        HashFunction hashFunc = null;

        if (hashType == HASH_TYPE.SHA1) {
            hashFunc = sha1;
        } else if (hashType == HASH_TYPE.MD5) {
            hashFunc = md5;
        }

        HashCode hc = hashFunc.newHasher().putUnencodedChars(clearText).hash();

        return hc.toString();
    }

    public String encryptSHA1(String clearText) {
        return encrypt(clearText, HASH_TYPE.SHA1);
    }
}
