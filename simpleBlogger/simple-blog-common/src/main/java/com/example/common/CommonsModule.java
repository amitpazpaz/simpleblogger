package com.example.common;

import com.google.inject.AbstractModule;
import org.atteo.moonshine.global_modules.GlobalModule;

@GlobalModule
public class CommonsModule extends AbstractModule {

    @Override
    protected void configure() {

        bind(EncryptionUtils.class);

    }
}
