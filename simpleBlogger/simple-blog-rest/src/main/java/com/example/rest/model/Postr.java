package com.example.rest.model;


import java.util.Date;

public class Postr extends BaseEntityr {

    public String subject;

    public String post;

    public String userName;

    public Long userId;


    public Postr(){
        super();
    }
    public Postr(Long id, Date createdAt, Date updatedAt, String subject, String post,Long userId,String userName) {
        super(id, createdAt, updatedAt);
        this.subject = subject;
        this.post = post;
        this.userId = userId;
        this.userName = userName;
    }
}
