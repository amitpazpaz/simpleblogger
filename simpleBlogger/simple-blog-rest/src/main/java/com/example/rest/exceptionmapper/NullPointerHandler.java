package com.example.rest.exceptionmapper;

import com.example.entities.OperationResponse;
import com.example.entities.OperationResult;
import com.example.rest.exception.MissingFieldsException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NullPointerHandler  implements ExceptionMapper<MissingFieldsException> {

    @Override
    public Response toResponse(MissingFieldsException e) {
        OperationResponse operationResponse = new OperationResponse();
        operationResponse.setState(OperationResult.ERROR);
        operationResponse.setDetails(e.getMessage());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_JSON).entity(operationResponse).build();

    }
}
