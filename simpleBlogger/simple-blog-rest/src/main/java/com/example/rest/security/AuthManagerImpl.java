package com.example.rest.security;


import com.example.common.EncryptionUtils;
import com.example.dao.user.UserDao;
import com.example.entities.User;
import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import com.googlecode.genericdao.search.Search;
import org.atteo.moonshine.jta.Transactional;

/**
 * Implementation of the {@link AuthManager} interface.
 */
public class AuthManagerImpl implements AuthManager {
    @Inject
    UserDao userDao;

    @Inject
    EncryptionUtils encryptionUtils;

    @Override
    public User getCurrent() {
        throw new IllegalArgumentException("Not implemented yet");
    }

    @Override
    @Transactional
    public User authenticate(String principal, String credentials) {

        Search search = new Search(User.class);
        search.addFilterEqual("email",principal);
        User u = userDao.searchUnique(search);

        Preconditions.checkNotNull(u, "Could not find user [" + principal + "]");


        if (u.credentials != null &&
                u.credentials.equals(encryptionUtils.encryptSHA1(credentials))) {
            return u;
        } else {
            return null;
        }
    }
}
