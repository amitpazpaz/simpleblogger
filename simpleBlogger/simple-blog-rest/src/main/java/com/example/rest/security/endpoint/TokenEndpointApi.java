package com.example.rest.security.endpoint;

import com.example.rest.security.annotations.RequiresAuthentication;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 * API for token related methods
 */

public interface TokenEndpointApi {
    @POST
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public Response authorize(@Context HttpServletRequest request) throws OAuthSystemException;

    /**
     * Logout identity by wiping out all sessions associated with the invoker
     * @return
     * @throws org.apache.oltu.oauth2.common.exception.OAuthSystemException
     */
    @POST
    @Path("logout")
    @RequiresAuthentication
    public Response logout();
}
