package com.example.rest.security.annotations;

import com.google.common.base.Preconditions;

import java.lang.annotation.Annotation;

/**
 * Base support class for implementations that reads and processes JSR-175 annotations.
 */
public abstract class AnnotationHandler {

    /**
     * The type of annotation this handler will process.
     */
    protected Class<? extends Annotation> annotationClass;

    /**
     * Constructs an <code>AnnotationHandler</code> that processes annotations of the
     * specified type. Immediately calls {@link #setAnnotationClass(Class)}.
     *
     * @param annotationClass the type of annotation this handler will process.
     */
    public AnnotationHandler(Class<? extends Annotation> annotationClass) {
        setAnnotationClass(annotationClass);
    }

    /**
     * Sets the type of annotation this handler will inspect and process.
     *
     * @param annotationClass the type of annotation this handler will process.
     * @throws IllegalArgumentException if the argument is <code>null</code>.
     */
    protected void setAnnotationClass(Class<? extends Annotation> annotationClass)
            throws IllegalArgumentException {
        Preconditions.checkNotNull(annotationClass, "annotationClass arg cannot be null.");
        this.annotationClass = annotationClass;
    }

    /**
     * Returns the type of annotation this handler inspects and processes.
     *
     * @return the type of annotation this handler inspects and processes.
     */
    public Class<? extends Annotation> getAnnotationClass() {
        return this.annotationClass;
    }
}
