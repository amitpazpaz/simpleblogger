package com.example.rest.exception;


import java.util.ArrayList;
import java.util.List;

//TODO: Javadoc or delete if unnecessary.
public class MissingFieldsException extends RuntimeException {

    private final List<String> fieldNames = new ArrayList<String>();

    public static MissingFieldsException create(){
        return new MissingFieldsException();
    }


    public MissingFieldsException addField(final String name){
        fieldNames.add(name);
        return this;
    }

    public List<String> missingFields() {
        return fieldNames;
    }

}
