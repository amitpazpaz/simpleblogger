package com.example.rest.ehcache;

import com.example.rest.ehcache.EhCacheManager;
import com.example.rest.ehcache.api.CacheManager;
import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Configuration;
import org.atteo.evo.config.XmlDefaultValue;
import org.atteo.moonshine.TopLevelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * EhCache Service.
 * <p/>
 * Maintaining the configuration and lifecycle of the {@link EhCacheManager}.
 */
@XmlRootElement(name = "ehcache")
public class EhCacheService extends TopLevelService {
    private final Logger log = LoggerFactory.getLogger(EhCacheService.class);

    @XmlElement
    @XmlDefaultValue("0")
    private Integer timeToIdleSeconds;
    @XmlElement
    @XmlDefaultValue("0")
    private Integer timeToLiveSeconds;
    @XmlElement
    @XmlDefaultValue("10000")
    private Long maxEntriesLocalHeap;

    net.sf.ehcache.CacheManager cacheMgr;

    private class EhCacheManagerProvider implements Provider<CacheManager> {
        @Override
        public CacheManager get() {
            Configuration config = new Configuration();
            CacheConfiguration cacheConfiguration = new CacheConfiguration();
            cacheConfiguration.setTimeToIdleSeconds(timeToIdleSeconds);
            cacheConfiguration.setTimeToLiveSeconds(timeToLiveSeconds);
            cacheConfiguration.setMaxEntriesLocalHeap(maxEntriesLocalHeap);
            config.setDefaultCacheConfiguration(cacheConfiguration);
            cacheMgr = net.sf.ehcache.CacheManager.create(config);
            EhCacheManager ehCacheManager = new EhCacheManager(cacheMgr);

            return ehCacheManager;
        }
    }

    @Override
    public Module configure() {
        return new AbstractModule() {
            @Override
            protected void configure() {
                bind(CacheManager.class).toProvider(new EhCacheManagerProvider()).in(Singleton.class);
            }
        };
    }

    public void stop() {
        try {
            cacheMgr.shutdown();
        } catch (Throwable t) {
            log.warn("Unable to gracefully shutdown CacheManager, ignoring shutdown.", t);
        }
    }
}
