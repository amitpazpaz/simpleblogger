package com.example.rest.security.endpoint;


import com.example.entities.User;
import com.example.rest.ehcache.api.Cache;
import com.example.rest.ehcache.api.CacheManager;
import com.example.rest.ehcache.api.Caches;
import com.example.rest.security.AccessToken;
import com.example.rest.security.CurrentUser;
import com.example.rest.security.endpoint.Common;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import com.example.rest.security.AuthManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.security.Identity;
import java.util.ArrayList;
import java.util.List;

@Path("/token")
public class TokenEndpoint implements TokenEndpointApi {
    public static final String INVALID_CLIENT_DESCRIPTION = "Client authentication failed (e.g., unknown client, no client authentication included, or unsupported authentication method).";
    @Inject
    private AuthManager authManager;

    @Inject
    private CacheManager cacheManager;

    @Inject
    Provider<CurrentUser> currentIdentityProvider;

    @Override
    public Response authorize(HttpServletRequest request) throws OAuthSystemException {
        OAuthTokenRequest oauthRequest = null;

        OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());

        try {
            oauthRequest = new OAuthTokenRequest(request);

            // check if clientid is valid
            if (!Common.CLIENT_ID.equals(oauthRequest.getClientId())) {
                OAuthResponse response =
                        OAuthASResponse.errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                                .setError(OAuthError.TokenResponse.INVALID_CLIENT).setErrorDescription(INVALID_CLIENT_DESCRIPTION)
                                .buildJSONMessage();

                return Response.status(response.getResponseStatus()).entity(response.getBody()).build();
            }

            // check if client_secret is valid
            if (!Common.CLIENT_SECRET.equals(oauthRequest.getClientSecret())) {
                OAuthResponse response =
                        OAuthASResponse.errorResponse(HttpServletResponse.SC_UNAUTHORIZED)
                                .setError(OAuthError.TokenResponse.UNAUTHORIZED_CLIENT).setErrorDescription(INVALID_CLIENT_DESCRIPTION)
                                .buildJSONMessage();

                return Response.status(response.getResponseStatus()).entity(response.getBody()).build();
            }

            User user = null;
            if (oauthRequest.getParam(OAuth.OAUTH_GRANT_TYPE).equals(GrantType.PASSWORD.toString())) {
                try {
                     user = authManager.authenticate(oauthRequest.getUsername(), oauthRequest.getPassword());
                } catch (Exception e) {

                }

                if (user == null) {
                    OAuthResponse response = OAuthASResponse
                            .errorResponse(HttpServletResponse.SC_BAD_REQUEST)
                            .setError(OAuthError.TokenResponse.INVALID_GRANT)
                            .setErrorDescription("invalid username or password")
                            .buildJSONMessage();

                    return Response.status(response.getResponseStatus()).entity(response.getBody()).build();
                }
            } else {
                throw OAuthProblemException.error(OAuthError.TokenResponse.UNSUPPORTED_GRANT_TYPE);
            }

            AccessToken accessToken = new AccessToken(oauthIssuerImpl.accessToken(), 3600, user);

            Cache<String, AccessToken> tokenCache = cacheManager.getCache(Caches.ACCESS_TOKENS.name());
            tokenCache.put(accessToken.getAccessToken(), accessToken);

            OAuthResponse response = OAuthASResponse
                    .tokenResponse(HttpServletResponse.SC_OK)
                    .setAccessToken(accessToken.getAccessToken())
                    .setExpiresIn(accessToken.getExpiresIn().toString())
                    .buildJSONMessage();
            return Response.status(response.getResponseStatus()).entity(response.getBody()).build();

        } catch (OAuthProblemException e) {
            OAuthResponse res = OAuthASResponse.errorResponse(HttpServletResponse.SC_BAD_REQUEST).error(e)
                    .buildJSONMessage();
            return Response.status(res.getResponseStatus()).entity(res.getBody()).build();
        }
    }

    @Override
    public Response logout() {
        Cache<String, AccessToken> tokenCache = cacheManager.getCache(Caches.ACCESS_TOKENS.name());
        List<String> tokensToDelete = new ArrayList<String>();
        for (AccessToken accessToken : tokenCache.values()) {
            if (accessToken.getUser().getId().equals(currentIdentityProvider.get().getUser().getId())) {
                tokensToDelete.add(accessToken.getAccessToken());
            }
        }

        for (String tokenToDelete : tokensToDelete) {
            tokenCache.remove(tokenToDelete);
        }

        return Response.status(Response.Status.OK).build();
    }
}