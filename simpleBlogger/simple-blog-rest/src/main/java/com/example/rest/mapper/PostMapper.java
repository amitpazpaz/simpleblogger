package com.example.rest.mapper;


import com.example.dao.post.PostDao;
import com.example.dao.user.UserDao;
import com.example.entities.Post;
import com.example.rest.MappingExecption;
import com.example.rest.model.Postr;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;


import javax.inject.Inject;
import java.util.List;

public class PostMapper implements Mapper<com.example.rest.model.Postr,Post> {
    @Inject
    PostDao postDao;

    @Inject
    UserDao userDao;

    @Override
    public Post fromREST(Postr postr) throws MappingExecption {

        Post post = new Post(postr.subject,postr.post);
        post.user = userDao.find(postr.userId);

           return post;
    }

    @Override
    public Postr toREST(Post post) throws MappingExecption {

        Long userId = null;

        if(post.user != null)
            userId = post.user.getId();
        Postr postr =  new Postr(post.getId(), post.getCreatedAt(), post.getUpdatedAt(),
                post.subject, post.post, userId, post.user.name);

        return postr;

    }

    public List<Postr> convertListToRest(List<Post> posts){

        return Lists.newArrayList(Iterables.transform(posts, postTransformFunction));
    }

    Function<Post, Postr> postTransformFunction = new Function<Post, Postr>() {
        public Postr apply(Post post) {
            try {
                return PostMapper.this.toREST(post);
            } catch (MappingExecption mappingExecption) {
                //TODO: do something intelligent here
                return null;
            }
        }
    };
}
