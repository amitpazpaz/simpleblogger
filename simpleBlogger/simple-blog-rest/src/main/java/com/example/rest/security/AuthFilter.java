package com.example.rest.security;


import com.example.entities.User;
import com.example.rest.ehcache.api.Cache;
import com.example.rest.ehcache.api.CacheManager;
import com.example.rest.ehcache.api.Caches;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.ParameterStyle;
import org.apache.oltu.oauth2.rs.request.OAuthAccessResourceRequest;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Singleton
public class AuthFilter implements Filter {
    @Inject
    Injector injector;

    @Inject
    Provider<CurrentUser> currentUserProvider;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        OAuthAccessResourceRequest oauthRequest = null;
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        try {
            oauthRequest = new OAuthAccessResourceRequest(httpServletRequest, ParameterStyle.HEADER);
            // Get the access token
            String accessToken = oauthRequest.getAccessToken();
            CacheManager cacheManager = injector.getInstance(CacheManager.class);
            Cache<String, AccessToken> cache = cacheManager.getCache(Caches.ACCESS_TOKENS.name());
            AccessToken at = cache.get(accessToken);

            if (at != null) {
                User i = at.getUser();
                currentUserProvider.get().setUser(i);
            }

        } catch (OAuthSystemException | OAuthProblemException e) {
            //TODO:Handle exceptions...
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
