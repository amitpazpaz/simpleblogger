package com.example.rest.mapper;


import com.example.entities.BaseEntity;
import com.example.rest.MappingExecption;
import com.example.rest.model.BaseEntityr;

public interface Mapper<T extends BaseEntityr,S extends BaseEntity> {

    public S fromREST(T baseEntityr) throws MappingExecption;
    public T toREST(S baseEntity) throws MappingExecption;

}
