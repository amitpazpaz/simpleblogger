package com.example.rest.ehcache.api;

import java.util.Collection;
import java.util.Set;

/**
 * A generic cache interface for storing objects, primarily to increase and improve application performance.
 * <p/>
 * This interface provides an abstraction API on top of an underlying cache framework's cache instance
 * such as Google's Guava or more robust distributed cache mechanism such as EhCache or Redis.
 */
public interface Cache<K, V> {
    /**
     * Store a new cache entry.
     *
     * @param key   key to identify the object being stored.
     * @param value value to be stored in the cache.
     * @return the previous value associated with the given {@code key} or {@code null} if there was no previous value
     * @throws CacheException in case there was a problem accessing the underlying cache system
     */
    public V put(K key, V value) throws CacheException;

    /**
     * Returns a cached value by the specified {@code key} or {@code null} if there is no entry for that {@code key}.
     *
     * @param key key associated with the value that was previous added with
     * @return cached object or {@code null} if there is no entry for the specified {@code key}
     * @throws CacheException in case there was a problem accessing the underlying cache system
     */
    public V get(K key) throws CacheException;

    /**
     * Remove a cache entry that corresponds to the specified key.
     *
     * @param key the key of the entry to be removed.
     * @return cached object or {@code null} if there is no entry for the specified {@code key}
     * @throws CacheException in case there was a problem accessing the underlying cache system
     */
    public V remove(K key) throws CacheException;

    /**
     * Clear all entries in the cache.
     *
     * @ throws CacheException in case there was a problem accessing the underlying cache system
     */
    public void clear() throws CacheException;

    /**
     * Returns the number of entries in the cache.
     *
     * @return the number of entries in the cache.
     */
    public int size();

    /**
     * Returns a view of all the keys for entries contained in this cache.
     *
     * @return a view of all the keys for entries contained in this cache.
     */
    public Set<K> keys();

    /**
     * Returns a view of all of the values contained in this cache.
     *
     * @return a view of all of the values contained in this cache.
     */
    public Collection<V> values();
}
