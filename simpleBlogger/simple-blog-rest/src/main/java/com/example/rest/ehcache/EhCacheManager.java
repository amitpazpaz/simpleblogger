package com.example.rest.ehcache;


import com.example.rest.ehcache.api.Cache;
import com.example.rest.ehcache.api.CacheException;
import com.example.rest.ehcache.api.CacheManager;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * EhCache {@link CacheManager} implementation.
 */
public class EhCacheManager implements CacheManager {
    private static final Logger log = LoggerFactory.getLogger(EhCacheManager.class);

    /**
     * The wrapped cache manager required to create {@link Cache caches}
     */
    private net.sf.ehcache.CacheManager manager;

    public EhCacheManager(net.sf.ehcache.CacheManager manager) {
        Preconditions.checkNotNull(manager, "EhCache's CacheManager cannot be null.");
        this.manager = manager;
    }


    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        log.trace("Acquiring EhCache instance named [{}", name);

        try {
            net.sf.ehcache.Ehcache cache = manager.getEhcache(name);
            if (cache == null) {
                log.info("Cache with name [{}] doesnt yet exist, creating now.", name);
                this.manager.addCache(name);
                cache = manager.getCache(name);

                log.info("Added EhCache named [{}]", name);
            } else {
                log.info("Using existing EHCache named [{}", cache.getName());
            }
            return new EhCache<K, V>(cache);
        } catch (net.sf.ehcache.CacheException e) {
            throw new CacheException(e);
        }
    }
}
