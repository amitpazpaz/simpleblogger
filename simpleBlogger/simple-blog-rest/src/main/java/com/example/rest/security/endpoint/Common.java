package com.example.rest.security.endpoint;

public final class Common {
    private Common() {
    }

    public static final String CLIENT_ID = "outbrain";
    public static final String CLIENT_SECRET = "outbrain";
    //TODO: Read somehow from a centralized configuration
    public static final String ACCESS_TOKEN_ENDPOINT = "http://localhost:9090/api/token";
}
