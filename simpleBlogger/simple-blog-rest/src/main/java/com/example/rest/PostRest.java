package com.example.rest;

import com.example.dao.post.PostDao;
import com.example.dao.user.UserDao;
import com.example.entities.Post;
import com.example.rest.mapper.PostMapper;
import com.example.rest.model.Postr;
import com.example.rest.security.CurrentUser;
import com.example.rest.security.exceptions.AuthorizationException;
import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import com.googlecode.genericdao.search.Search;
import org.apache.lucene.search.Query;
import org.atteo.moonshine.jta.Transactional;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.query.dsl.QueryBuilder;


import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.ws.rs.Path;
import java.util.List;

@Path("/posts")
public class PostRest implements PostApi {

    @Inject
    PostDao postDao;

    @Inject
    UserDao userDao;

    @Inject
    PostMapper postMapper;

    @Inject
    CurrentUser currentUser;

    @Inject
    EntityManager entityManager;

    @Override
    @Transactional
    public List<Postr> list( int maxResult, int page) throws MappingExecption {

        Search search = new Search(Post.class);
        search.addSort("createdAt",true);
        search.setPage(page);
        search.setMaxResults(maxResult);

        List<Post> posts = postDao.search(search);
        return postMapper.convertListToRest(posts);
    }

    @Override
    @Transactional
    public Postr add(Postr postr) throws MappingExecption{

        postr.userId = currentUser.getUser().getId();

        Post post = fromREST(postr);
        return toREST(postDao.save(post));
    }

    @Override
    @Transactional
    public Postr update(Postr postr) throws MappingExecption {


        Post foundPost = postDao.find(postr.id);
        Preconditions.checkNotNull(foundPost, new EntityNotFoundException());

        if(foundPost.user.getId() != currentUser.getUser().getId())
            throw new AuthorizationException("Modifying other users posts is prohibited ");

        Post modifiedPost = fromREST(postr);
        modifiedPost.setId(foundPost.getId());
        return toREST(postDao.save(modifiedPost));
    }

    @Override
    @Transactional
    public void delete(Postr postr) throws MappingExecption {

        if(postr.userId != currentUser.getUser().getId())
            throw new SecurityException("Removing other users posts is prohibited ");

        Post post = postDao.find(postr.id);

        if(post!=null)
            postDao.remove(post);
    }

    @Override
    @Transactional
    public Postr get(String postId) throws MappingExecption {

        Post post = postDao.find(Long.parseLong(postId));
        return  toREST(post);
    }

    @Override
    @Transactional
    public List<Postr> search(String query) throws MappingExecption {

        FullTextEntityManager index = org.hibernate.search.jpa.Search.getFullTextEntityManager(entityManager);

        QueryBuilder builder = index.getSearchFactory().buildQueryBuilder().forEntity(Post.class).get();
        Query qr = builder.keyword().fuzzy().onFields("subject", "post")
                .matching(query).createQuery();

        javax.persistence.Query persistenceQuery = index.createFullTextQuery(qr, Post.class);
        List<Post> result = persistenceQuery.getResultList();

        return  postMapper.convertListToRest(result);
    }

    private Post fromREST(Postr postr) throws MappingExecption{
        return postMapper.fromREST(postr);
    }

    private Postr toREST(Post post) throws MappingExecption{
        return postMapper.toREST(post);
    }


}
