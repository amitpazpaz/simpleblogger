package com.example.rest.security;

import com.example.entities.User;
/**
 * Authentication manager
 *
 * Implementation lives in the server module.
 */
public interface AuthManager {
    /**
     * Get the current authenticated {@link User} or null if no identity found.
     *
     * @return the current authenticated {@link User}
     */
    public User getCurrent();

    /**
     * Perform identity authentication by the provided principal and credentials
     *
     * @param principal the principal to auth, i.e identity name
     * @param credentials The clear text credentials to auth, i.e clear password
     * @return <code>Identity</code> instance upon success auth, or null if failed
     */
    public User authenticate(String principal, String credentials);


}
