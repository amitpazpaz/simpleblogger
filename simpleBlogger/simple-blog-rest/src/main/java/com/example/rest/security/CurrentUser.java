package com.example.rest.security;

import com.example.entities.User;
import com.google.inject.servlet.RequestScoped;

/**
 * A wrapper to the current authenticated {@link User}
 */
@RequestScoped
public class CurrentUser {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
