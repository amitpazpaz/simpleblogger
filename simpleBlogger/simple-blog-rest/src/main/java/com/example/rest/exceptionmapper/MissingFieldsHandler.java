package com.example.rest.exceptionmapper;

import com.example.entities.OperationResponse;
import com.example.entities.OperationResult;
import com.example.rest.exception.MissingFieldsException;
import com.google.common.base.Joiner;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/*
 * TODO should be mapped to json not plan text
 */
@Provider
public class MissingFieldsHandler implements ExceptionMapper<MissingFieldsException> {
    @Override
    public Response toResponse(MissingFieldsException missingFieldsException) {
        String message = "Required Fields are missing form request: ";
        message += Joiner.on(",").join(missingFieldsException.missingFields());
        OperationResponse operationResponse = new OperationResponse();
        operationResponse.setState(OperationResult.ERROR);
        operationResponse.setDetails(message);
        return Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON).entity(operationResponse).build();

    }
}
