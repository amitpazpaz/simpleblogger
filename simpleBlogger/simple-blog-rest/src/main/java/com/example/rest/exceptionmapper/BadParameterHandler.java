package com.example.rest.exceptionmapper;


import com.example.entities.OperationResponse;
import com.example.entities.OperationResult;
import com.example.rest.exception.BadParameterException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class BadParameterHandler implements ExceptionMapper<BadParameterException> {
    @Override
    public Response toResponse(BadParameterException e) {
        StringBuilder sb = new StringBuilder();
        for (BadParameterException.Parameter parameter : e.getParameters()){
            sb.append(parameter.getName()).append(": ").append(parameter.getMessage()).append("\n");
        }
        OperationResponse operationResponse = new OperationResponse();
        operationResponse.setState(OperationResult.ERROR);
        operationResponse.setDetails(sb.toString());
        return Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON).entity(operationResponse).build();

    }
}
