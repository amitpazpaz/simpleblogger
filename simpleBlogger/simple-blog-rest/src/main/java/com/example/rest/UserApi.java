package com.example.rest;

import com.example.entities.Post;
import com.example.entities.User;
import com.example.rest.model.Postr;
import com.example.rest.model.Userr;
import org.jboss.resteasy.annotations.providers.jaxb.Formatted;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import java.util.List;

@Produces({MediaType.APPLICATION_JSON})
public interface UserApi {

    @GET
    @Formatted
    public List<Userr> list(@DefaultValue("-1") @QueryParam("from") int from,
                         @DefaultValue("25") @QueryParam("pageSize") int perPage) throws MappingExecption;

    @POST
    @Formatted
    @Consumes({MediaType.APPLICATION_JSON})
    //@ResponseStatus(Response.Status.CREATED)
    public Userr add(Userr Userr) throws MappingExecption;

    @PUT
    @Formatted
    @Consumes(MediaType.APPLICATION_JSON)
    public Userr update(Userr userr) throws MappingExecption;

    @GET
    @Path("/{id}")
    @Formatted
    public Userr get(@PathParam("id") String userId)throws MappingExecption;

    @DELETE
    @Path("/{id}")
    @Formatted
    public Userr delete(@PathParam("id") String userId)throws MappingExecption;

    @GET
    @Path("/{id}/posts")
    List<Postr> listPosts(@PathParam("id") String userId,
                                @DefaultValue("-1") @QueryParam("from") int from,
                                @DefaultValue("-1") @QueryParam("pageSize") int perPage)throws MappingExecption;


}
