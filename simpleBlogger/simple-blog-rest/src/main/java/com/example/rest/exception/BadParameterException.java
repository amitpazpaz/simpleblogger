package com.example.rest.exception;

import java.util.ArrayList;
import java.util.List;

public class BadParameterException extends RuntimeException {

    private final List<Parameter> parametersMessages = new ArrayList<Parameter>();

    public static BadParameterException create(){
        return new BadParameterException();
    }
    public BadParameterException addParameter(String name, String message){
        parametersMessages.add(new Parameter(name,message));
        return this;
    }

    public List<Parameter> getParameters (){
        return parametersMessages;
    }


    private BadParameterException(){

    }

    public static class Parameter{
        private final String name;
        private final String message;

        private Parameter(String name, String message) {
            this.name = name;
            this.message = message;
        }

        public String getName() {
            return name;
        }

        public String getMessage() {
            return message;
        }
    }
}
