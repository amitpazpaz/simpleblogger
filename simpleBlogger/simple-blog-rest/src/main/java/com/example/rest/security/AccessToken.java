package com.example.rest.security;

import com.example.entities.User;
import com.google.common.base.Preconditions;

import java.util.Date;

/**
 * AccessToken object
 */
public class AccessToken {

    /**
     * The access token itself
     */
    private String accessToken;

    /**
     * The time the token was issued
     */
    private Date createdAt;

    /**
     * Expiration time (in MS) since the creation date of the token.
     */
    private Integer expiresIn;

    /**
     * The {@link User} instance attached to the token
     */
    private User user;

    public AccessToken(String accessToken, Integer expiresIn, User user) {
        Preconditions.checkNotNull(accessToken);
        Preconditions.checkNotNull(expiresIn);
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.user = user;
        this.createdAt = new Date();
    }

    public String getAccessToken() {
        return accessToken;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public User getUser() {
        return user;
    }
}
