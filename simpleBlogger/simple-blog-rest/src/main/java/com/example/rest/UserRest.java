package com.example.rest;

import com.beust.jcommander.internal.Lists;
import com.example.common.EncryptionUtils;
import com.example.dao.user.UserDao;
import com.example.entities.User;
import com.example.rest.exception.BadParameterException;
import com.example.rest.mapper.PostMapper;
import com.example.rest.mapper.UserMapper;
import com.example.rest.model.Postr;
import com.example.rest.model.Userr;
import com.google.inject.Inject;
import com.googlecode.genericdao.search.Search;
import org.atteo.moonshine.jta.Transactional;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.Path;
import java.util.List;

@Path("/users")
public class UserRest implements UserApi {

    @Inject
    UserDao userDao;

    @Inject
    UserMapper userMapper;

    @Inject
    PostMapper postMapper;

    @Inject
    EncryptionUtils encryptionUtils;


    @Override
    @Transactional
    public List<Userr> list(@DefaultValue("-1") int from, @DefaultValue("25") int perPage) {

        List<User> result = userDao.findAll();

        return userMapper.convertListToRest(result);
    }

    @Override
    @Transactional
    public List<Postr> listPosts(String userId, @DefaultValue("-1") int from, @DefaultValue("-1") int perPage) {

        Search search = new Search(User.class);
        try{

            User user = userDao.find(Long.parseLong(userId));
            if(user!=null) {

                List<Postr> posts = postMapper.convertListToRest(Lists.newArrayList(user.posts));
                return posts;
            }

        }catch (NumberFormatException ex){

            BadParameterException badParameterException = BadParameterException.create();
            badParameterException.addParameter("userId","Error while attempting to convert user id to number");
            throw badParameterException;
        }


        return null;
    }

    @Override
    @Transactional
    public Userr add(Userr userr) throws MappingExecption {

        User user = fromREST(userr);
        return toREST(userDao.save(user));
    }

    @Override
    @Transactional
    public Userr update(Userr userr) throws MappingExecption {

        boolean changed = false;

        User user = userDao.find(userr.id);
        if(userr.name!=null && !user.name.isEmpty()) {
            user.name = userr.name;
            changed = true;
        }

        if(userr.email!=null && !user.email.isEmpty()) {
            user.email = userr.email;
            changed=true;
        }

        if(userr.credentials != null && !userr.credentials.isEmpty()) {
            user.credentials = encryptionUtils.encryptSHA1(userr.credentials);
            changed = true;
        }

        if(changed)
            user.setChangeDate();

        return toREST(userDao.merge(user));
    }

    @Override
    @Transactional
    public Userr get(String userId) throws MappingExecption {

        User user = userDao.find(Long.parseLong(userId));
        return toREST(user);
    }

    @Override
    @Transactional
    public Userr delete(String userId) throws MappingExecption {
        User user = userDao.find(Long.parseLong(userId));
        userDao.remove(user);
        //TODO: return response
        return null;
    }



    User fromREST(Userr userr) throws MappingExecption{
        return userMapper.fromREST(userr);
    }

    Userr toREST(User user) throws MappingExecption{
        return userMapper.toREST(user);
    }
}
