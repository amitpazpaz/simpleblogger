package com.example.rest.ehcache.api;

/**
 * Thrown when there is a cache related exception
 */
public class CacheException extends RuntimeException {
    public CacheException(Throwable cause) {
        super(cause);
    }
}
