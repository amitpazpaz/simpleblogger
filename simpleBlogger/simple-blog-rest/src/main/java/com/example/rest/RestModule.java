package com.example.rest;

import com.example.rest.mapper.PostMapper;
import com.example.rest.mapper.UserMapper;
import com.google.inject.AbstractModule;
import org.atteo.moonshine.global_modules.GlobalModule;

@GlobalModule
public class RestModule extends AbstractModule {

    @Override
    protected void configure() {

        bind(UserMapper.class);
        bind(PostMapper.class);

    }
}
