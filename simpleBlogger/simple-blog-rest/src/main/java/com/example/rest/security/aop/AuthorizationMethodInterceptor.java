package com.example.rest.security.aop;


import com.example.rest.security.CurrentUser;
import com.example.rest.security.annotations.AnnotationMethodInterceptor;
import com.example.rest.security.exceptions.AuthorizationException;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.aopalliance.intercept.MethodInvocation;

public class AuthorizationMethodInterceptor extends AnnotationMethodInterceptor {

    @Inject
    public AuthorizationMethodInterceptor(Provider<CurrentUser> currentUserProvider) {
        this(new AuthorizationAnnotationHandler(currentUserProvider));
    }

    public AuthorizationMethodInterceptor(AuthorizationAnnotationHandler handler) {
        super(handler);
    }

    public CurrentUser getCurrentIdentity() {
        return ((AuthorizationAnnotationHandler) getHandler()).getUser();
    }

    /**
     * Invokes the specified method (<code>methodInvocation.{@link org.aopalliance.intercept.MethodInvocation#proceed proceed}()</code>
     * if authorization is allowed by first
     * calling {@link #assertAuthorized(org.aopalliance.intercept.MethodInvocation) assertAuthorized}.
     */
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        assertAuthorized(methodInvocation);
        return methodInvocation.proceed();
    }


    /**
     * Ensures the calling <code>Identity</code> is authorized to execute the specified <code>MethodInvocation</code>.
     * <p/>
     * As this is an AnnotationMethodInterceptor, this implementation merely delegates to the internal
     * {@link AuthorizationAnnotationHandler} by first acquiring the annotation by
     * calling {@link #getAnnotation(org.aopalliance.intercept.MethodInvocation) getAnnotation(methodInvocation)} and then calls
     * {@link AuthorizationAnnotationHandler#assertAuthorized(java.lang.annotation.Annotation) handler.assertAuthorized(annotation)}.
     *
     * @param mi the <code>MethodInvocation</code> to check to see if it is allowed to proceed/execute.
     * @throws AuthorizationException if the method invocation is not allowed to continue/execute.
     */
    public void assertAuthorized(MethodInvocation mi) throws AuthorizationException {
        try {
            ((AuthorizationAnnotationHandler) getHandler()).assertAuthorized(getAnnotation(mi));
        } catch (AuthorizationException ae) {
            // Annotation handler doesn't know why it was called, so add the information here if possible.
            // Don't wrap the exception here since we don't want to mask the specific exception, such as
            // UnauthenticatedException etc.
            if (ae.getCause() == null)
                ae.initCause(new AuthorizationException("Not authorized to invoke method: " + mi.getMethod()));
            throw ae;
        }
    }
}