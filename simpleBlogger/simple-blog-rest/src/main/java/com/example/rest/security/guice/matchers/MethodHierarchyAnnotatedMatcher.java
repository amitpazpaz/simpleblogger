package com.example.rest.security.guice.matchers;

import com.example.rest.security.util.ReflectionUtils;
import com.google.inject.matcher.AbstractMatcher;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Guice matcher for matching annotated methods by the specified annotation,
 *
 * The behavior also respect methods defined in declaring class's method interfaces that has a method which correspond
 * to the inspected one.
 */
public class MethodHierarchyAnnotatedMatcher extends AbstractMatcher<Method> {
    private final Class<? extends Annotation> annClass;

    /**
     * Constructor.
     *
     * @param annClass
     */
    public MethodHierarchyAnnotatedMatcher(final Class<? extends Annotation> annClass) {
        this.annClass = annClass;
    }

    /**
     * Search for the method signature into all the interface annotated with a
     * given annotation.
     */
    public boolean matches(Method method) {
        return (ReflectionUtils.getAnnotation(method, annClass) != null);
    }
}
