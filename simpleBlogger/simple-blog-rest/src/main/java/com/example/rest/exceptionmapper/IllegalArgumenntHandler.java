package com.example.rest.exceptionmapper;

import com.example.entities.OperationResponse;
import com.example.entities.OperationResult;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class IllegalArgumenntHandler implements ExceptionMapper<IllegalArgumentException> {
    @Override
    public Response toResponse(IllegalArgumentException e) {
        OperationResponse operationResponse = new OperationResponse();
        operationResponse.setState(OperationResult.ERROR);
        operationResponse.setDetails(e.getMessage());
        return Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON).entity(operationResponse).build();

    }
}
