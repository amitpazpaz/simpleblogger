package com.example.rest.exceptionmapper;


import com.example.entities.OperationResponse;
import com.example.entities.OperationResult;
import com.example.rest.exception.MissingFieldsException;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class ValidationExceptionHandler implements ExceptionMapper<ConstraintViolationException> {

String RESPONSE_MESSAGE = "VALIDATION_ERROR";
        @Override
        public Response toResponse(ConstraintViolationException e) {
            OperationResponse operationResponse = new OperationResponse();
            operationResponse.setState(OperationResult.ERROR);
            operationResponse.setDetails(RESPONSE_MESSAGE);
            return Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON).entity(operationResponse).build();

        }
    }

