package com.example.rest.security.aop;


import com.example.rest.security.CurrentUser;
import com.example.rest.security.annotations.AnnotationHandler;
import com.example.rest.security.annotations.RequiresAuthentication;
import com.example.rest.security.exceptions.AuthorizationException;
import com.google.inject.Provider;
import com.example.entities.User;

import java.lang.annotation.Annotation;

/**
 *  handler for authorization.
 */
public  class AuthorizationAnnotationHandler extends AnnotationHandler {
    Provider<CurrentUser> currentUserProvider;

    public AuthorizationAnnotationHandler(Provider<CurrentUser> currentUserProvider) {
        this(currentUserProvider, RequiresAuthentication.class);
    }

    public AuthorizationAnnotationHandler(Provider<CurrentUser> currentIdentityProvider, Class<? extends Annotation> annotationClass) {
        super(annotationClass);
        setCurrentIdentityProvider(currentIdentityProvider);
    }

    /**
     * Returns the {@link User} associated with the currently-executing code.
     *
     * @return the Identity associated with the currently-executing code.
     */
    protected CurrentUser getUser() {
        return getCurrentUserProvider().get();
    }

    public void setCurrentIdentityProvider(Provider<CurrentUser> currentUserProvider) {
        this.currentUserProvider = currentUserProvider;
    }

    public Provider<CurrentUser> getCurrentUserProvider() {
        return currentUserProvider;
    }

    /**
     * Ensures the calling {@link User} is authorized to execute based on the directive(s)
     * found in the given annotation.
     * <p/>
     *
     * @param a the <code>Annotation</code> to check for performing an authorization check.
     * @throws com.example.rest.security.exceptions.AuthorizationException if the class/instance/method is not allowed to proceed/execute.
     */
    public void assertAuthorized(Annotation a) throws AuthorizationException {
        if (!(a instanceof RequiresAuthentication)) return;

        CurrentUser currentIdentity = getUser();

        if (currentIdentity.getUser() == null)
            throw new AuthorizationException("User is not authenticated.");
    }
}
