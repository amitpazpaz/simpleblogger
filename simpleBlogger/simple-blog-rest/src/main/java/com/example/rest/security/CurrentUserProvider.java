package com.example.rest.security;

import com.google.inject.Provider;
import com.example.entities.User;
/**
 * Provides the current authenticated {@link User} instance or null if there's none.
 */
public class CurrentUserProvider implements Provider<CurrentUser> {
    @Override
    public CurrentUser get() {
        return new CurrentUser();
    }
}
