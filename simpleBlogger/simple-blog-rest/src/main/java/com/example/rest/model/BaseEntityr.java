package com.example.rest.model;


import java.text.SimpleDateFormat;
import java.util.Date;

public class BaseEntityr {

    public Long id;
    public String createdAt;
    public String updatedAt;

    public BaseEntityr(Long id, Date createdAt, Date updatedAt) {

        this.id = id;
        if(createdAt!=null)
            this.createdAt = createdAt.toString();
        if(updatedAt!=null)
            this.updatedAt = updatedAt.toString();
    }

    public BaseEntityr(){

    }
}
