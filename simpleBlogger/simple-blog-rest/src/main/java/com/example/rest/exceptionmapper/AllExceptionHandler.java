package com.example.rest.exceptionmapper;

import com.example.entities.OperationResponse;
import com.example.entities.OperationResult;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Rest Exception handler for any exception  (Catch all missed)
 */
@Provider
public class AllExceptionHandler implements ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception e) {
        OperationResponse operationResponse = new OperationResponse();
        operationResponse.setState(OperationResult.ERROR);
        operationResponse.setDetails(e.getMessage());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_JSON).entity(operationResponse).build();
    }
}
