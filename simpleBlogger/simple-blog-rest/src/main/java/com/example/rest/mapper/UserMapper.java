package com.example.rest.mapper;


import com.example.common.EncryptionUtils;
import com.example.dao.user.UserDao;
import com.example.entities.User;
import com.example.rest.MappingExecption;
import com.example.rest.model.Userr;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.List;

public class UserMapper implements Mapper<com.example.rest.model.Userr,User>{

    @Inject
    UserDao userDao;

    @Inject
    EncryptionUtils encryptionUtils;


    @Override
    public User fromREST(Userr userr) throws MappingExecption {
        String encryptedPass = null;

        if(userr.credentials !=null && !userr.credentials.isEmpty())
            encryptedPass = encryptionUtils.encryptSHA1(userr.credentials);
        User newUser = new User(userr.name, userr.email, encryptedPass, null);
        return newUser;
    }

    @Override
    public Userr toREST(User user) throws MappingExecption {
        Userr userr =  new Userr(user.getId(),user.getCreatedAt(),
                user.getUpdatedAt(),user.name,user.email);

        //Never disclose credentials(even hashed) override just in case.
        userr.credentials = "*******ENCRYPTED********";

        return userr;
    }

    public List<Userr> convertListToRest(List<User> users){

        return Lists.newArrayList(Iterables.transform(users, userTransformFunction));
    }

    Function<User, Userr> userTransformFunction = new Function<User, Userr>() {
        public Userr apply(User user) {
            try {
                return UserMapper.this.toREST(user);
            } catch (MappingExecption mappingExecption) {
                //TODO: do something intelligent here
                return null;
            }
        }
    };
}
