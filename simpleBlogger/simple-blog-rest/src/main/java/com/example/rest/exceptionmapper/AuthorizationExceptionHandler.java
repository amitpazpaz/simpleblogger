package com.example.rest.exceptionmapper;


import com.example.entities.OperationResponse;
import com.example.entities.OperationResult;
import com.example.rest.security.exceptions.AuthorizationException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Handle authorization exception
 */
@Provider
public class AuthorizationExceptionHandler implements ExceptionMapper<AuthorizationException> {
    @Override
    public Response toResponse(AuthorizationException e) {
        OperationResponse operationResponse = new OperationResponse();
        operationResponse.setState(OperationResult.ERROR);
        operationResponse.setDetails(e.getMessage());
        return Response.status(Response.Status.UNAUTHORIZED).type(MediaType.APPLICATION_JSON).entity(operationResponse).build();
    }
}
