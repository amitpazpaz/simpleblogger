package com.example.rest;


public class MappingExecption extends RuntimeException {

    public MappingExecption(String message, Throwable cause) {
        super(message, cause);
    }
}
