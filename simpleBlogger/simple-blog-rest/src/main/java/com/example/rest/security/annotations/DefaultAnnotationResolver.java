package com.example.rest.security.annotations;

import com.example.rest.security.util.ReflectionUtils;
import com.google.common.base.Preconditions;
import org.aopalliance.intercept.MethodInvocation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Default {@code AnnotationResolver} implementation that merely inspects the
 * {@link org.aopalliance.intercept.MethodInvocation MethodInvocation}'s {@link org.aopalliance.intercept.MethodInvocation#getMethod() target method},
 * and returns {@code targetMethod}.{@link java.lang.reflect.Method#getAnnotation(Class) getAnnotation(class)}.
 * <p/>
 *
 * This implementation relies on the default JDK's {@link java.lang.reflect.Method#getAnnotation(Class) Method.getAnnotation(class)}
 * logic which may not be robust enough for some cases such as class hierarchy traversal.
 */
public class DefaultAnnotationResolver implements AnnotationResolver {

    /**
     * Returns {@code methodInvocation.}{@link org.aopalliance.intercept.MethodInvocation#getMethod() getMethod()}.{@link java.lang.reflect.Method#getAnnotation(Class) getAnnotation(clazz)}.
     *
     * @param mi    the intercepted method to be invoked.
     * @param clazz the annotation class to use to find an annotation instance on the method.
     * @return the discovered annotation or {@code null} if an annotation instance could not be
     * found.
     */
    public Annotation getAnnotation(MethodInvocation mi, Class<? extends Annotation> clazz) {
        Preconditions.checkNotNull(mi, "mi arg cannot be null.");
        Method m = mi.getMethod();
        Preconditions.checkNotNull(m, MethodInvocation.class.getName() + " parameter incorrectly constructed.  getMethod() returned null");

        Annotation anno = ReflectionUtils.getAnnotation(m, clazz);

        return anno == null ? mi.getThis().getClass().getAnnotation(clazz) : anno;
    }
}
