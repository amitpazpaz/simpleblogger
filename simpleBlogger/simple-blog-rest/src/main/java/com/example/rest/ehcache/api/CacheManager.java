package com.example.rest.ehcache.api;

/**
 * provides and maintains the lifecycles of {@link Cache} instances.
 */
public interface CacheManager {

    /**
     * Acquires the cache with the specified <code>name</code>.
     *
     * If a cache doesn't yet exist with that name then a new one will be created with the given name and returned.
     *
     * @param name the name of the cache to acquire.
     * @return the Cache with the given name
     * @throws CacheException if there is an error acquiring the Cache instance.
     */
    public <K, V> Cache<K, V> getCache(String name) throws CacheException;
}