package com.example.rest.security.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class ReflectionUtils {
    /**
     * Inspect and return annotation if it exists on the specified method,
     * <p/>
     * The logic inspects all interfaces of the declared class's method's,
     * If one of the interfaces has the same method signature it will be inspected too
     *
     * @param method
     * @param annotation
     * @return annotation if presented
     */
    public static Annotation getAnnotation(Method method, Class<? extends Annotation> annotation) {
        if (method.isAnnotationPresent(annotation)) {
            return method.getAnnotation(annotation);
        }

        Annotation foundAnnotation = null;
        Class<?> type = method.getDeclaringClass();
        Class<?>[] iTypes = type.getInterfaces();
        for (Class<?> iType : iTypes) {
            Method iMethod = null;

            try {
                iMethod = iType.getDeclaredMethod(method.getName(), method
                        .getParameterTypes());
            } catch (SecurityException e) {

            } catch (NoSuchMethodException e) {

            }

            if (iMethod != null && iMethod.isAnnotationPresent(annotation)) {
                foundAnnotation = iMethod.getAnnotation(annotation);
                break;
            }
        }

        return foundAnnotation;
    }
}
