package com.example.rest.ehcache;


import com.example.rest.ehcache.api.Cache;
import com.example.rest.ehcache.api.CacheException;
import com.google.common.base.Preconditions;
import net.sf.ehcache.Element;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * {@link Cache} implementation that wraps an {@link net.sf.ehcache.Ehcache} instance.
 */
public class EhCache<K, V> implements Cache<K, V> {
    private static final Logger log = LoggerFactory.getLogger(EhCache.class);

    /**
     * The wrapped Ehcache instance.
     */
    private net.sf.ehcache.Ehcache cache;

    /**
     * Constructor
     *
     * @param cache - delegate EhCache instance to this cache wrapper.
     */
    public EhCache(net.sf.ehcache.Ehcache cache) {
        Preconditions.checkNotNull(cache, "Cache cannot be null.");
        this.cache = cache;
    }

    @Override
    public V put(K key, V value) throws CacheException {
        log.trace("Putting entry from cache [{}] by key [{}]", cache.getName(), key);

        try {
            V prev = get(key);
            Element element = new Element(key, value);
            cache.put(element);
            return prev;
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public V get(K key) throws CacheException {
        try {
            log.trace("Getting entry from cache [{}] by key [{}]", cache.getName(), key);

            if (key == null)
                return null;

            Element element = cache.get(key);
            if (element == null) {
                log.trace("Element for [" + key + "] is null.");
                return null;
            } else {
                //noinspection unchecked
                return (V) element.getObjectValue();
            }
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public V remove(K key) throws CacheException {
        log.trace("Removing entry from cache [{}] by key [{}]", cache.getName(), key);

        try {
            V prev = get(key);
            cache.remove(key);
            return prev;
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public void clear() throws CacheException {
        log.trace("Clearing all cache entries from cache [{}]", cache.getName());
        try {
            cache.removeAll();
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public int size() {
        try {
            return cache.getSize();
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public Set<K> keys() {
        try {
            @SuppressWarnings({"unchecked"})
            List<K> keys = cache.getKeys();
            if (!CollectionUtils.isEmpty(keys)) {
                return Collections.unmodifiableSet(new LinkedHashSet<K>(keys));
            } else {
                return Collections.emptySet();
            }
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    @Override
    public Collection<V> values() {
        try {
            @SuppressWarnings({"unchecked"})
            List<K> keys = cache.getKeys();
            if (!CollectionUtils.isEmpty(keys)) {
                List<V> values = new ArrayList<V>(keys.size());
                for (K key : keys) {
                    V value = get(key);
                    if (value != null) {
                        values.add(value);
                    }
                }
                return Collections.unmodifiableList(values);
            } else {
                return Collections.emptyList();
            }
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    /**
     * Returns the size (in bytes) that this EhCache is using in memory (RAM), or <code>-1</code> if that
     * number is unknown or cannot be calculated.
     *
     * @return the size (in bytes) that this EhCache is using in memory (RAM), or <code>-1</code> if that
     *         number is unknown or cannot be calculated.
     * @throws CacheException in case there was a problem accessing the underlying cache system
     */
    public long getMemoryUsage() {
        try {
            return cache.calculateInMemorySize();
        }
        catch (Throwable t) {
            return -1;
        }
    }

    /**
     * Returns the size (in bytes) that this EhCache's memory store is using (RAM), or <code>-1</code> if
     * that number is unknown or cannot be calculated.
     *
     * @return the size (in bytes) that this EhCache's memory store is using (RAM), or <code>-1</code> if
     *         that number is unknown or cannot be calculated.
     * @throws CacheException in case there was a problem accessing the underlying cache system
     */
    public long getMemoryStoreSize() {
        try {
            return cache.getMemoryStoreSize();
        }
        catch (Throwable t) {
            throw new CacheException(t);
        }
    }

    /**
     * Returns the size (in bytes) that this EhCache's disk store is consuming or <code>-1</code> if
     * that number is unknown or cannot be calculated.
     *
     * @return the size (in bytes) that this EhCache's disk store is consuming or <code>-1</code> if
     *         that number is unknown or cannot be calculated.
     * @throws CacheException in case there was a problem accessing the underlying cache system
     */
    public long getDiskStoreSize() {
        try {
            return cache.getDiskStoreSize();
        } catch (Throwable t) {
            throw new CacheException(t);
        }
    }


    @Override
    public String toString() {
        return "EhCache [" + cache.getName() + "]";
    }
}
