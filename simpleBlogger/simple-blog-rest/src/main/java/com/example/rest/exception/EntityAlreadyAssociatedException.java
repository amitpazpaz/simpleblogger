package com.example.rest.exception;

public class EntityAlreadyAssociatedException extends RuntimeException {

    public static EntityAlreadyAssociatedException factory(String parent, String child){
        return new EntityAlreadyAssociatedException(parent, child);
    }

    private EntityAlreadyAssociatedException(String parentEntityName, String childEntityName){
        super("Entity [" + parentEntityName + "] is already associated with [" + childEntityName + "]");
    }
}
