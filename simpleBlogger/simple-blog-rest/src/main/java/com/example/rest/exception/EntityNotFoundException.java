package com.example.rest.exception;

public class EntityNotFoundException extends RuntimeException {

    public static EntityNotFoundException factory(String name){
        return new EntityNotFoundException(name);
    }

    public static EntityNotFoundException factory (Class<?> clzz){
        return new EntityNotFoundException(clzz.getName());
    }

    private String entityName;

    private EntityNotFoundException(String name){
        super(name);
        this.entityName = name;
    }

    public String getEntityName(){
        return entityName;
    }


}
