package com.example.rest;

import com.example.entities.Post;
import com.example.rest.model.Postr;
import com.example.rest.security.annotations.RequiresAuthentication;
import org.jboss.resteasy.annotations.providers.jaxb.Formatted;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Produces({MediaType.APPLICATION_JSON})
public interface PostApi {
    @GET
    @Formatted
    public List<Postr> list(@DefaultValue("10") @QueryParam("maxResult") int maxResult,
                           @DefaultValue("0") @QueryParam("page") int page) throws MappingExecption;

    @POST
    @Formatted
    @Consumes({MediaType.APPLICATION_JSON})
    @RequiresAuthentication
    public Postr add(Postr post) throws MappingExecption;

    @PUT
    @Formatted
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresAuthentication
    public Postr update(Postr postr) throws MappingExecption;

    @DELETE
    @Formatted
    @Consumes(MediaType.APPLICATION_JSON)
    @RequiresAuthentication
    public void delete(Postr postr) throws MappingExecption;

    @GET
    @Path("/{id}")
    @Formatted
    public Postr get(@PathParam("id") String postId) throws MappingExecption;;

    @GET
    @Path("/search")
    @Formatted
    public List<Postr> search(@QueryParam("query") String query) throws MappingExecption;;

}
