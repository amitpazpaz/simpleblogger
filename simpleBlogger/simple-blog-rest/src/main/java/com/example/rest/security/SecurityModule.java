package com.example.rest.security;

import com.example.rest.security.annotations.AnnotationMethodInterceptor;
import com.example.rest.security.aop.AuthorizationMethodInterceptor;
import com.example.rest.security.guice.matchers.MethodHierarchyAnnotatedMatcher;
import com.google.inject.matcher.Matchers;
import com.google.inject.servlet.ServletModule;
import com.google.inject.servlet.ServletScopes;
import org.atteo.moonshine.global_modules.GlobalModule;

/**
 * Register AOP interceptors.
 */
@GlobalModule
public class SecurityModule extends ServletModule {
    @Override
    protected void configureServlets() {

        bind(AuthManager.class).to(AuthManagerImpl.class);
        bind(CurrentUserProvider.class).in(ServletScopes.REQUEST);
        bind(CurrentUser.class).toProvider(CurrentUserProvider.class).in(ServletScopes.REQUEST);
        bind(AuthFilter.class);
        filter("/api/*").through(AuthFilter.class);

        AuthorizationMethodInterceptor authorizationMethodInterceptor = new AuthorizationMethodInterceptor(getProvider(CurrentUser.class));
        requestInjection(authorizationMethodInterceptor);
        bindAuthInterceptor(authorizationMethodInterceptor);

    }

    protected final void bindAuthInterceptor(final AnnotationMethodInterceptor methodInterceptor) {
        bindInterceptor(Matchers.any(), new MethodHierarchyAnnotatedMatcher(methodInterceptor.getHandler().getAnnotationClass()), methodInterceptor);
    }
}