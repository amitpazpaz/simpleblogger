package com.example.rest;

import com.beust.jcommander.internal.Lists;
import com.example.entities.Post;
import com.example.rest.model.Postr;
import com.example.rest.model.Userr;
import com.jayway.restassured.http.ContentType;

import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.jayway.restassured.RestAssured.expect;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;


public class PostRestTest extends BaseRestTest {


        Postr post;
        Postr loadedPost = null;

        public PostRestTest() {
            super(Postr.class);
        }

        @Override
        public void addBase() {

            Userr userr = (Userr)addEntity(new Userr(new Long(1), new Date(), new Date(), "user1", "user1@gmail.com"), "users", Userr.class);


            for (int i = 1; i <= 4; i++) {

                addEntity(new Postr(null, new Date(), new Date(), "Some Subject", "Some post", userr.id, "user1"), "posts");
            }
            Postr[] posts2 = (Postr[]) this.getCollection("posts", Postr[].class);
            List<Postr> posts = Lists.<Postr>newArrayList(posts2);

            assertThat(posts.size(), is(4));
            post = (Postr) posts.get(1);

            assertThat(post.subject, notNullValue());
            assertThat(post.post, notNullValue());
            assertThat(post.userId, notNullValue());

        }

        @Override
        public void getBase() {
            loadedPost =
                    expect().contentType(ContentType.JSON).statusCode(200).get(getURL("posts/" + post.id))
                            .as(Postr.class);

            assertThat(loadedPost.id, is(post.id));
            assertThat(loadedPost.subject, is(post.subject));
            assertThat(loadedPost.post, is(post.post));

            // not found
            expect().statusCode(Response.Status.BAD_REQUEST.getStatusCode()).get(getURL("posts/" + UUID.randomUUID()));
        }

        @Override
        public void updateBase() {
            // Update User
            String UPDATED_SUBJECT = "Updated Subject";
            Postr updated = new Postr();
            updated.subject = UPDATED_SUBJECT;
            updated.post = loadedPost.post;
            updated.id = loadedPost.id;
            updated.userId = loadedPost.userId;
            updated.userName = loadedPost.userName;

            updateModel(updated, "posts");
            loadedPost =
                    expect().contentType(ContentType.JSON).statusCode(200).get(getURL("posts/" + loadedPost.id))
                            .as(Postr.class);
            assertThat(updated.id, is(loadedPost.id));
            assertThat(updated.subject, is(UPDATED_SUBJECT));
            assertThat(updated.userId, is(loadedPost.userId));

        }

        @Override
        public void removeBase() {

            //TODO: implement after getting hired to outBrain.. :)
        }
    }

