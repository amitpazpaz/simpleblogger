package com.example.rest;

import com.example.common.EncryptionUtils;
import com.example.dao.user.UserDao;
import com.example.entities.User;
import com.example.rest.model.BaseEntityr;
import com.example.rest.security.endpoint.Common;
import com.google.inject.Inject;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.atteo.moonshine.jta.Transaction;
import org.atteo.moonshine.tests.MoonshineConfiguration;
import org.atteo.moonshine.tests.MoonshineTest;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.expect;
import static com.jayway.restassured.RestAssured.given;

@MoonshineConfiguration(oneRequestPerClass = true)
public abstract class BaseRestTest <C extends BaseEntityr> extends MoonshineTest {

    private final Class<C> entityClazz;

    public BaseRestTest(Class<C> entityClazz) {
        this.entityClazz = entityClazz;
    }

    @Inject
    UserDao userDao;

    @Inject
    EncryptionUtils encryptionUtils;

    public static final String SERVER_URL = "http://localhost:9090";
    public static final String REST_URL = SERVER_URL + "/api/";
    public String ACCESS_TOKEN = null;
    public Map<Long, C> models = new HashMap<>();

    public <M> M getCollection(String subUrl,Class<M> clazz) {
        return expect().contentType(ContentType.JSON).statusCode(200).get(getURL(subUrl)).as(clazz);
    }

    public static String getURL(String resource) {
        return REST_URL + resource;
    }

    @Before
    public void beforeMethod() {
        ACCESS_TOKEN = authenticate("test@outbrain.com", "200217677");
    }

    @Test
    public void baseTest() {

        // Add Entity
        addBase();

        // Retrieve Entity
        getBase();

        // Update Base
        updateBase();

        // remove Base
        removeBase();

    }

    public abstract void addBase();

    public abstract void getBase();

    public abstract void updateBase();

    public abstract void removeBase();

    public C addEntity(C entityToSend, String subUrl){
            C model =
                    givenAuth(ACCESS_TOKEN).body(entityToSend).contentType(ContentType.JSON).expect().statusCode(200).post(getURL(subUrl))
                            .as(entityClazz);

            models.put(model.id, model);
            return model;

        }

    public C addEntity(C entityToSend, String subUrl, Class<C> overrEntityClazz){
        C model = null;
        if(overrEntityClazz != null) {
            model =
                    givenAuth(ACCESS_TOKEN).body(entityToSend).contentType(ContentType.JSON).expect().statusCode(200).post(getURL(subUrl))
                            .as(overrEntityClazz);
            return model;
        }
        if(model != null)
            models.put(model.id, model);

        return model;
    }

    public C updateModel(C modelToUpdate, String subUrl) {
        C model =
                givenAuth(ACCESS_TOKEN).body(modelToUpdate).contentType(ContentType.JSON).expect().contentType(ContentType.JSON)
                        .statusCode(200).put(getURL(subUrl)).as(entityClazz);

        return model;
    }

    public Response deleteModel(String subUrl) {
        return given().contentType(ContentType.JSON).expect().statusCode(200).delete(getURL(subUrl));
    }

    public RequestSpecification givenAuth(String token) {
        return given().header("Authorization", "Bearer " + token);
    }

    /*
        Helper method to create and authenticate a user
        Performing Creation,deletion and modifications require previous authentication

        @return User access token
     */
    public String authenticate(final String email,final String password) {

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {

                User user = new User(email, email, encryptionUtils.encryptSHA1(password), null);
                userDao.save(user);
            }
        });

        //now authenticate the newly created user

        OAuthClientRequest request = null;
        try {
            request = OAuthClientRequest
                    .tokenLocation(Common.ACCESS_TOKEN_ENDPOINT)
                    .setGrantType(GrantType.PASSWORD)
                    .setClientId(Common.CLIENT_ID)
                    .setClientSecret(Common.CLIENT_SECRET)
                    .setUsername(email)
                    .setPassword(password)
                    .buildBodyMessage();

            OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
            OAuthJSONAccessTokenResponse response = oAuthClient.accessToken(request);
            return response.getAccessToken();
        } catch (OAuthSystemException | OAuthProblemException e) {
            return null;
        }
    }


}
