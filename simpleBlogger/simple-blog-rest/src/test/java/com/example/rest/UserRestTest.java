package com.example.rest;


import com.beust.jcommander.internal.Lists;
import com.example.entities.User;
import com.example.rest.model.Userr;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.jayway.restassured.http.ContentType;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.jayway.restassured.RestAssured.expect;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class UserRestTest extends BaseRestTest<Userr> {
    Userr user;
    Userr loadedUser = null;

    public UserRestTest() {
        super(Userr.class);
    }

    @Override
    public void addBase() {

        for (int i = 1; i <= 4; i++) {

            addEntity(new Userr(new Long(i), new Date(), new Date(), "user" + i, "user" + i + "@gmail.com"), "users");
        }

        List<Userr> users = Lists.newArrayList(getCollection("users", Userr[].class));
        //The fifth user is the one we authenticated with.
        assertThat(users.size(), is(5));
        user = users.get(1);

        assertThat(user.name, notNullValue());
        assertThat(user.email, notNullValue());
        assertThat(user.id, notNullValue());

    }

    @Override
    public void getBase() {
        loadedUser =
                expect().contentType(ContentType.JSON).statusCode(200).get(getURL("users/" + user.id))
                        .as(Userr.class);

        assertThat(loadedUser.id, is(user.id));
        assertThat(loadedUser.name, is(user.name));
        assertThat(loadedUser.email, is(user.email));

        // not found
        expect().statusCode(404).get(getURL("users/" + UUID.randomUUID()));
    }

    @Override
    public void updateBase() {
        // Update User
        Userr updated = new Userr();
        updated.email = "user" + loadedUser.id + "@outbrain.com";
        updated.id = user.id;

        updateModel(updated, "users");
        loadedUser =
                expect().contentType(ContentType.JSON).statusCode(200).get(getURL("users/" + loadedUser.id))
                        .as(Userr.class);
        assertThat(loadedUser.id, is(user.id));
        assertThat(loadedUser.name, is(user.name));
        assertThat(loadedUser.email, is("user" + loadedUser.id + "@outbrain.com"));

    }

    @Override
    public void removeBase() {

    //TODO: implement after getting hired to outBrain.. :)
    }
}