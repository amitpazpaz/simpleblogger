package com.example.rest;


import com.example.common.EncryptionUtils;
import com.example.dao.user.UserDao;
import com.example.entities.User;
import com.example.rest.security.endpoint.Common;
import com.google.inject.Inject;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.atteo.moonshine.jta.Transaction;
import org.atteo.moonshine.tests.MoonshineConfiguration;
import org.atteo.moonshine.tests.MoonshineTest;
import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import  com.example.rest.security.AuthManager;

/**
 * Unit test for {@link AuthManager}
 */

@MoonshineConfiguration
public class AuthManagerTest extends MoonshineTest {

    @Inject
    AuthManager authManager;

    @Inject
    static UserDao userDao;

    @Inject
    static EncryptionUtils encryptionUtils;

    static User user;

    @BeforeClass
    public static void setupUser() {
        Transaction.require(new Transaction.Runnable() {

            @Override
            public void run() {

                user = new User("user1", "user1@test.com", encryptionUtils.encryptSHA1("1111"), null);
                userDao.save(user);
            }
        });
    }
            @Test
            public void testAuthenticate() throws Exception {

                assertThat(authManager.authenticate("user1@test.com", "wrongPassw0rd!@#"), nullValue());
                assertThat(authManager.authenticate("user1@test.com", "1111"), notNullValue());

            }

            @Test
            public void testBearerToken() throws Exception {

                OAuthClientRequest request = null;
                try {
                    request = OAuthClientRequest
                            .tokenLocation(Common.ACCESS_TOKEN_ENDPOINT)
                            .setGrantType(GrantType.PASSWORD)
                            .setClientId(Common.CLIENT_ID)
                            .setClientSecret(Common.CLIENT_SECRET)
                            .setUsername(user.email)
                            .setPassword(user.credentials)
                            .buildBodyMessage();

                    OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
                    OAuthJSONAccessTokenResponse response = oAuthClient.accessToken(request);

                    assertThat(response.getAccessToken(), Matchers.notNullValue());

                } catch (OAuthSystemException | OAuthProblemException e) {
                }
            }

            @Test
            public void testWrongCredentials() {

                OAuthClientRequest request = null;

                try {
                    request = OAuthClientRequest
                            .tokenLocation(Common.ACCESS_TOKEN_ENDPOINT)
                            .setGrantType(GrantType.PASSWORD)
                            .setClientId(Common.CLIENT_ID)
                            .setClientSecret(Common.CLIENT_SECRET)
                            .setUsername(user.email)
                            .setPassword("WrongCredentials!")
                            .buildBodyMessage();

                    OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
                    OAuthJSONAccessTokenResponse response = oAuthClient.accessToken(request);

                    assertThat(response.getAccessToken(), Matchers.nullValue());

                } catch (OAuthSystemException | OAuthProblemException e) {
                }

            }
        }