package com.example.entities;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.Set;

@Entity
public class User extends BaseEntity
        implements Serializable  {

    public User(){}

    public User(String name, String email, String credentials, Set<Post> posts) {
        this.name = name;
        this.email = email;
        this.posts = posts;
        this.credentials = credentials;
    }

    @NotBlank(message = "User name is required")
    @Size(min=3, max=20)
    public String name;

    @NotBlank(message = "Email is required")
    @Email(message = "Email must be of form name@address")
    @Size(min=3, max=30)
    @Column(unique = true)
    public String email;

    @Size(min=40, max=40)
    @Lob
    @Column( length = 40)
    @Pattern(regexp="[a-fA-F0-9]{40}")
    @NotBlank(message = "Password is required and must be encrypted")
    public String credentials;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    public Set<Post> posts;

}
