package com.example.entities;

import org.apache.solr.analysis.LowerCaseFilterFactory;
import org.apache.solr.analysis.SnowballPorterFilterFactory;
import org.apache.solr.analysis.StandardTokenizerFactory;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

@Entity
@Indexed
@AnalyzerDef(name = "customanalyzer",
        tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
        filters = {
                @TokenFilterDef(factory = LowerCaseFilterFactory.class),
                @TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = {
                        @Parameter(name = "language", value = "English")
                })
        })
public class Post extends BaseEntity {

    @NotBlank(message = "Message is required")
    @Field
    @Analyzer(definition = "customanalyzer")
    public String subject;

    @NotBlank(message = "Post is required")
    @Lob
    @Column( length = 100000 )
    @Field
    @Analyzer(definition = "customanalyzer")
    public String post;

    @ManyToOne(optional = false, targetEntity = User.class)
    @JoinColumn(name = "user_id")
    @IndexedEmbedded
    public User user;

    public Post(){

    }

    public Post(String subject,String post){
        this.subject = subject;
        this.post = post;
    }


}
