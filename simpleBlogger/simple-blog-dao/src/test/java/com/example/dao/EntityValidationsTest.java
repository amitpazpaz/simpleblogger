package com.example.dao;


import com.example.common.EncryptionUtils;
import com.example.dao.post.PostDao;
import com.example.dao.user.UserDao;
import com.example.entities.User;
import org.atteo.moonshine.jta.Transaction;
import org.atteo.moonshine.tests.MoonshineTest;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EntityValidationsTest extends MoonshineTest {

    @Inject
    private UserDao userDao;

    @Inject
    private PostDao postDao;

    @Inject
    private EncryptionUtils encryptionUtils;

    final User u = new User();


    @Test(expected=ConstraintViolationException.class)
    public void validateUserEmailNoAt() throws ConstraintViolationException {


        u.name = "Joe cocker";
        u.email = "joemoe";


        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {

                userDao.save(u);

            }
        });
    }

    @Test(expected=ConstraintViolationException.class)
    public void validateUserEmailMailBlank() throws ConstraintViolationException {

        u.email = "";

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {

                userDao.save(u);

            }
        });


    }

    @Test(expected=ConstraintViolationException.class)
    public void validateUserEmailTooShort() throws ConstraintViolationException {

        u.email = "j@";

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {

                userDao.save(u);

            }
        });
    }


    @Test(expected=ConstraintViolationException.class)
    public void validateUserNameShort() throws ConstraintViolationException{

        u.name = "wr";
        u.email = "pazamit@gmail.com";


        Transaction.require(new Transaction.Runnable() {
                @Override
                public void run() {

                        userDao.save(u);
                }
            });
    }

    @Test(expected=ConstraintViolationException.class)
    public void validateUserNameLong() throws ConstraintViolationException{

        u.name = "mary-poppins Supercalifragilisticexpialidocious";

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {

                userDao.save(u);
            }
        });
    }

    @Test(expected=ConstraintViolationException.class)
    public void validateUserCredentials() throws ConstraintViolationException{

        u.name = "Amit Paz";
        u.email = "pazamit@gmail.com";
        u.credentials = "not a hash";
        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {

                userDao.save(u);
            }
        });

    }

    @Test
    public void validateUserCredentialsOkay(){

        u.name = "Amit Paz";
        u.email = "pazamit@gmail.com";
        u.credentials = encryptionUtils.encryptSHA1("hashme");

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {
                try {
                    userDao.save(u);
                }catch(ConstraintViolationException ex){
                    Assert.assertTrue(false);
                }
            }
        });

    }
}
