package com.example.dao;


import com.example.common.EncryptionUtils;
import com.example.dao.post.PostDao;
import com.example.dao.user.UserDao;
import com.example.entities.Post;
import com.example.entities.User;
import org.atteo.moonshine.jta.Transaction;
import org.atteo.moonshine.tests.MoonshineTest;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserPostsEntitiesCRUDTest extends MoonshineTest {

    @Inject
    private UserDao userDao;

    @Inject
    private PostDao postDao;

    @Inject
    EncryptionUtils encryptionUtils;

    @Test
    public void createUserWithPosts() {

        final User u = new User();
        final Post p = new Post();

        u.name = "Amit";
        u.email = "pazamit@gmail.com";
        u.credentials = encryptionUtils.encryptSHA1("password");

        p.user = u;
        p.subject = "Test me!!";
        p.post = "This is my basic tests";

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {
                userDao.save(u);
                postDao.save(p);
            }
        });

        final Long postId = p.getId();

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {

                Post post = postDao.find(postId);

                assertNotNull(post);
                assertEquals(post, p);
                assertNotNull(post.getCreatedAt());


            }
        });
    }


}
