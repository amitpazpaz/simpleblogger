/*
 * Copyright 2011 Atteo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.example.dao;

import javax.inject.Inject;

import com.example.dao.post.PostDao;
import com.example.dao.user.UserDao;
import com.example.entities.Post;
import com.example.entities.User;
import com.googlecode.genericdao.search.Search;
import org.atteo.moonshine.jta.Transaction;
import org.atteo.moonshine.tests.MoonshineTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BasicEntitiesCRUDTest extends MoonshineTest {

    @Inject
    private UserDao userDao;

    @Inject
    private PostDao postDao;

    @Test
    public void create() {

        final User u = new User();
        final Post p = new Post();

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {

                u.name = "Amit";
                u.email = "pazamit@gmail.com";
                userDao.save(u);

                p.user = u;
                p.subject = "Test me!!";
                p.post = "This is my basic tests";
                postDao.save(p);


            }
        });

        final Long uId = u.getId();
        final Long pId = p.getId();

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {
                Post pLoaded = postDao.find(pId);
                User uLoaded = userDao.find(uId);

                //check user
                assertNotNull(uLoaded);
                assertEquals(u.name, uLoaded.name);
                assertNotNull(uLoaded.getCreatedAt());

                //check post
                assertNotNull(pLoaded);
                assertEquals(p.subject, pLoaded.subject);
                assertNotNull(p.getCreatedAt());
            }
        });
    }

    @Test
    public void update() {
        final String newEmail = "pazamit@outbrain.com";
        final String newSubject = "Test me updated!!";

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {
                Search search = new Search(User.class);
                search.addFilterEqual("email","pazamit@gmail.com");
                User user =  userDao.searchUnique(search);
                user.email = newEmail;

                userDao.save(user);

                search = new Search(Post.class);
                search.addFilterEqual("subject","Test me!!");
                Post post =  postDao.searchUnique(search);
                post.subject = newSubject;

                postDao.save(post);

            }
        });

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {

                Search search = new Search(User.class);
                search.addFilterEqual("email","pazamit@outbrain.com");
                User user =  userDao.searchUnique(search);

                assertNotNull(user);
                assertEquals(user.email,newEmail);

                search = new Search(Post.class);
                search.addFilterEqual("subject",newSubject);
                Post post =  postDao.searchUnique(search);

                assertNotNull(user);
                assertEquals(post.subject,newSubject);

            }
        });
    }

    @Test
    public void vDelete(){

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {

                Search search = new Search(User.class);
                search.addFilterEqual("email", "pazamit@outbrain.com");
                User user = userDao.searchUnique(search);
                userDao.remove(user);

            }
        });

        Transaction.require(new Transaction.Runnable() {
            @Override
            public void run() {
                Search search = new Search(Post.class);
                Post post = postDao.searchUnique(search);
                assertNull(post);

                search = new Search(User.class);
                search.addFilterEqual("email", "pazamit@outbrain.com");
                User user = userDao.searchUnique(search);
                assertNull(user);

            }
        });

    }



}
