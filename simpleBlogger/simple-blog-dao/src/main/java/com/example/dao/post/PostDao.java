package com.example.dao.post;

import com.example.entities.Post;
import com.googlecode.genericdao.dao.jpa.GenericDAO;

import javax.persistence.EntityManager;

public interface PostDao extends GenericDAO<Post, Long> {
   public void setEntityManager(EntityManager em);
}