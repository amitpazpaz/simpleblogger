package com.example.dao.user;

import com.example.entities.EntityValidationException;
import com.example.entities.User;
import com.google.inject.Inject;
import com.googlecode.genericdao.dao.jpa.GenericDAOImpl;
import com.googlecode.genericdao.search.jpa.JPASearchProcessor;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;


public class UserDaoImpl extends GenericDAOImpl<User,Long> implements UserDao {

    @Inject
    public UserDaoImpl(EntityManager em,JPASearchProcessor jpaSearchProcessor){
        super.setEntityManager(em);
        super.setSearchProcessor(jpaSearchProcessor);
    }

    @Override
    public User find(Long id){

        User user = super.find(id);
        if(user == null) throw new EntityNotFoundException(String.format("User with id %s is not found!", id) );

        return user;
    }
}
