package com.example.dao.post;

import com.example.entities.Post;
import com.google.inject.Inject;
import com.googlecode.genericdao.dao.jpa.GenericDAOImpl;
import com.googlecode.genericdao.search.jpa.JPASearchProcessor;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;


public class PostDaoImpl extends GenericDAOImpl<Post,Long> implements PostDao {

    @Inject
    public PostDaoImpl(EntityManager em, JPASearchProcessor jpaSearchProcessor){
        super.setEntityManager(em);
        super.setSearchProcessor(jpaSearchProcessor);
    }

    @Override
    public Post find(Long id){

        Post post = super.find(id);
        if(post == null) throw new EntityNotFoundException(String.format("Post with id %s is not found!", id) );

        return post;
    }

}
