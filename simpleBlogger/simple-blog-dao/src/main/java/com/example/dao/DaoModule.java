package com.example.dao;

import com.example.dao.post.PostDao;
import com.example.dao.post.PostDaoImpl;
import com.example.dao.user.UserDao;
import com.example.dao.user.UserDaoImpl;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.googlecode.genericdao.search.jpa.JPAAnnotationMetadataUtil;
import com.googlecode.genericdao.search.jpa.JPASearchProcessor;
import org.atteo.moonshine.global_modules.GlobalModule;

@GlobalModule
public class DaoModule extends AbstractModule {

    @Override
    protected void configure() {

        bind(UserDao.class).to(UserDaoImpl.class);
        bind(PostDao.class).to(PostDaoImpl.class);

    }

    @Provides
    @Singleton
    JPASearchProcessor provideJPASearchProcessor(){

        JPAAnnotationMetadataUtil hibernateMetadataUtil = new JPAAnnotationMetadataUtil();
        JPASearchProcessor jpaSearchProcessor = new JPASearchProcessor(hibernateMetadataUtil);

        return jpaSearchProcessor;
    }
}
