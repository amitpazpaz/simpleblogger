package com.example.dao.user;

import com.example.entities.User;
import com.googlecode.genericdao.dao.jpa.GenericDAO;

import javax.persistence.EntityManager;

public interface UserDao extends GenericDAO<User, Long> {
   public void setEntityManager(EntityManager em);
}